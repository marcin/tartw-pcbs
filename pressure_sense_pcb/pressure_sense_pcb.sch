EESchema Schematic File Version 4
LIBS:pressure_sense_pcb-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5C08E946
P 1350 1250
F 0 "J1" H 1270 1467 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 1270 1376 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 1350 1250 50  0001 C CNN
F 3 "~" H 1350 1250 50  0001 C CNN
	1    1350 1250
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C08ED3B
P 2100 1050
F 0 "#PWR05" H 2100 800 50  0001 C CNN
F 1 "GND" H 2105 877 50  0000 C CNN
F 2 "" H 2100 1050 50  0001 C CNN
F 3 "" H 2100 1050 50  0001 C CNN
	1    2100 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 5C08ED9F
P 1775 1550
F 0 "#PWR03" H 1775 1400 50  0001 C CNN
F 1 "+5V" H 1790 1723 50  0000 C CNN
F 2 "" H 1775 1550 50  0001 C CNN
F 3 "" H 1775 1550 50  0001 C CNN
	1    1775 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 1250 1950 1050
Text Label 3575 1425 0    50   ~ 0
MISO
Wire Wire Line
	4000 1300 3825 1300
Wire Wire Line
	4525 1300 4700 1300
$Sheet
S 4000 1700 525  325 
U 5BF4FF81
F0 "sheet5BF4FF7B" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 1800 50 
F3 "Vout" I R 4525 1800 50 
F4 "IN" B L 4000 1925 50 
F5 "OUT" B R 4525 1925 50 
$EndSheet
Text Label 3675 1925 0    50   ~ 0
CLK
Wire Wire Line
	4000 1800 3825 1800
Wire Wire Line
	4525 1800 4700 1800
Text Label 3675 2450 0    50   ~ 0
CS0
Wire Wire Line
	4000 2950 3825 2950
Text Label 3825 2950 0    50   ~ 0
CS1
Wire Wire Line
	4525 2825 4700 2825
$Sheet
S 4000 3225 525  325 
U 5BF5088D
F0 "sheet5BF5087B" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 3325 50 
F3 "Vout" I R 4525 3325 50 
F4 "IN" B L 4000 3450 50 
F5 "OUT" B R 4525 3450 50 
$EndSheet
Wire Wire Line
	4000 3450 3825 3450
Text Label 3825 3450 0    50   ~ 0
CS2
Wire Wire Line
	4525 3325 4700 3325
Wire Wire Line
	4000 3975 3825 3975
Text Label 3825 3975 0    50   ~ 0
CS3
Wire Wire Line
	4525 3850 4700 3850
$Sheet
S 4000 4225 525  325 
U 5BF51D67
F0 "sheet5BF51D5C" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 4325 50 
F3 "Vout" I R 4525 4325 50 
F4 "IN" B L 4000 4450 50 
F5 "OUT" B R 4525 4450 50 
$EndSheet
Wire Wire Line
	4000 4450 3825 4450
Text Label 3825 4450 0    50   ~ 0
CS4
Wire Wire Line
	4525 4325 4700 4325
$Sheet
S 4000 4725 525  325 
U 5BF51D72
F0 "sheet5BF51D5D" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 4825 50 
F3 "Vout" I R 4525 4825 50 
F4 "IN" B L 4000 4950 50 
F5 "OUT" B R 4525 4950 50 
$EndSheet
Wire Wire Line
	4000 4950 3825 4950
Text Label 3825 4950 0    50   ~ 0
CS5
Wire Wire Line
	4525 4825 4700 4825
Wire Wire Line
	4000 5475 3825 5475
Text Label 3825 5475 0    50   ~ 0
CS6
Wire Wire Line
	4525 5350 4700 5350
$Sheet
S 4000 5725 525  325 
U 5BF51D88
F0 "sheet5BF51D5F" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 5825 50 
F3 "Vout" I R 4525 5825 50 
F4 "IN" B L 4000 5950 50 
F5 "OUT" B R 4525 5950 50 
$EndSheet
Wire Wire Line
	4000 5950 3825 5950
Text Label 3825 5950 0    50   ~ 0
CS7
Wire Wire Line
	4525 5825 4700 5825
Wire Wire Line
	3675 1925 4000 1925
Wire Wire Line
	3825 1800 3825 2325
Connection ~ 3825 1800
Wire Wire Line
	3825 1300 3825 975 
Connection ~ 3825 1300
$Comp
L power:+3.3V #PWR09
U 1 1 5BF5E435
P 3825 975
F 0 "#PWR09" H 3825 825 50  0001 C CNN
F 1 "+3.3V" H 3840 1148 50  0000 C CNN
F 2 "" H 3825 975 50  0001 C CNN
F 3 "" H 3825 975 50  0001 C CNN
	1    3825 975 
	1    0    0    -1  
$EndComp
Connection ~ 4700 1800
Wire Wire Line
	4700 975  4700 1300
Connection ~ 4700 1300
Wire Wire Line
	4700 1300 4700 1800
$Comp
L power:+5V #PWR010
U 1 1 5BF693BD
P 4700 975
F 0 "#PWR010" H 4700 825 50  0001 C CNN
F 1 "+5V" H 4715 1148 50  0000 C CNN
F 2 "" H 4700 975 50  0001 C CNN
F 3 "" H 4700 975 50  0001 C CNN
	1    4700 975 
	1    0    0    -1  
$EndComp
$Sheet
S 4000 1200 525  325 
U 5BF4E0A5
F0 "BLS" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 1300 50 
F3 "Vout" I R 4525 1300 50 
F4 "IN" B L 4000 1425 50 
F5 "OUT" B R 4525 1425 50 
$EndSheet
Wire Wire Line
	4525 5950 4825 5950
Connection ~ 4700 2825
Wire Wire Line
	4700 2825 4700 3325
Connection ~ 4700 3325
Wire Wire Line
	4700 3325 4700 3850
Connection ~ 4700 3850
Wire Wire Line
	4700 3850 4700 4325
Wire Wire Line
	4700 4825 4700 4325
Connection ~ 4700 4325
Wire Wire Line
	4700 5350 4700 4825
Connection ~ 4700 4825
Wire Wire Line
	4700 5825 4700 5350
Connection ~ 4700 5350
Wire Wire Line
	3825 2325 3775 2325
Wire Wire Line
	3775 2325 3775 2825
Wire Wire Line
	3775 5825 4000 5825
Wire Wire Line
	3775 2825 4000 2825
Connection ~ 3775 2825
Wire Wire Line
	3775 2825 3775 3325
Wire Wire Line
	3775 3325 4000 3325
Connection ~ 3775 3325
Wire Wire Line
	3775 3325 3775 3850
Wire Wire Line
	3775 3850 4000 3850
Connection ~ 3775 3850
Wire Wire Line
	3775 3850 3775 4325
Wire Wire Line
	3775 4325 4000 4325
Connection ~ 3775 4325
Wire Wire Line
	3775 4325 3775 4825
Wire Wire Line
	3775 4825 4000 4825
Connection ~ 3775 4825
Wire Wire Line
	3775 4825 3775 5350
Wire Wire Line
	3775 5350 4000 5350
Connection ~ 3775 5350
Wire Wire Line
	3775 5350 3775 5825
Wire Wire Line
	1950 1250 2250 1250
Wire Wire Line
	2250 1250 2250 1100
Wire Wire Line
	1950 1350 2250 1350
Wire Wire Line
	2250 1350 2250 1500
Text Label 2025 2200 0    50   ~ 0
CS6
$Comp
L power:+5V #PWR013
U 1 1 5C0B3EE5
P 9700 1025
F 0 "#PWR013" H 9700 875 50  0001 C CNN
F 1 "+5V" H 9715 1198 50  0000 C CNN
F 2 "" H 9700 1025 50  0001 C CNN
F 3 "" H 9700 1025 50  0001 C CNN
	1    9700 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1100 2450 1100
Wire Wire Line
	2450 1100 2450 1150
Wire Wire Line
	2450 1450 2450 1500
Wire Wire Line
	2450 1500 2250 1500
$Sheet
S 4000 2725 525  325 
U 5BF50882
F0 "sheet5BF5087A" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 2825 50 
F3 "Vout" I R 4525 2825 50 
F4 "IN" B L 4000 2950 50 
F5 "OUT" B R 4525 2950 50 
$EndSheet
$Sheet
S 4000 3750 525  325 
U 5BF50898
F0 "sheet5BF5087C" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 3850 50 
F3 "Vout" I R 4525 3850 50 
F4 "IN" B L 4000 3975 50 
F5 "OUT" B R 4525 3975 50 
$EndSheet
$Sheet
S 4000 5250 525  325 
U 5BF51D7D
F0 "sheet5BF51D5E" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 5350 50 
F3 "Vout" I R 4525 5350 50 
F4 "IN" B L 4000 5475 50 
F5 "OUT" B R 4525 5475 50 
$EndSheet
$Comp
L Device:C C5
U 1 1 5C10ED92
P 8225 1300
F 0 "C5" H 8340 1346 50  0000 L CNN
F 1 "0.1u" H 8340 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8263 1150 50  0001 C CNN
F 3 "~" H 8225 1300 50  0001 C CNN
	1    8225 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5C10F0B6
P 8525 1300
F 0 "C6" H 8640 1346 50  0000 L CNN
F 1 "0.1u" H 8640 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8563 1150 50  0001 C CNN
F 3 "~" H 8525 1300 50  0001 C CNN
	1    8525 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5C10F0FE
P 8775 1300
F 0 "C7" H 8890 1346 50  0000 L CNN
F 1 "0.1u" H 8890 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8813 1150 50  0001 C CNN
F 3 "~" H 8775 1300 50  0001 C CNN
	1    8775 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5C10F148
P 9025 1300
F 0 "C8" H 9140 1346 50  0000 L CNN
F 1 "0.1u" H 9140 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9063 1150 50  0001 C CNN
F 3 "~" H 9025 1300 50  0001 C CNN
	1    9025 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5C10F186
P 7950 1300
F 0 "C4" H 8065 1346 50  0000 L CNN
F 1 "0.1u" H 8065 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7988 1150 50  0001 C CNN
F 3 "~" H 7950 1300 50  0001 C CNN
	1    7950 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5C10F1C8
P 9275 1300
F 0 "C9" H 9390 1346 50  0000 L CNN
F 1 "0.1uF" H 9390 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9313 1150 50  0001 C CNN
F 3 "~" H 9275 1300 50  0001 C CNN
	1    9275 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5C10F2A5
P 7650 1300
F 0 "C3" H 7765 1346 50  0000 L CNN
F 1 "0.1u" H 7765 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7688 1150 50  0001 C CNN
F 3 "~" H 7650 1300 50  0001 C CNN
	1    7650 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5C10F325
P 7350 1300
F 0 "C2" H 7465 1346 50  0000 L CNN
F 1 "0.1u" H 7465 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7388 1150 50  0001 C CNN
F 3 "~" H 7350 1300 50  0001 C CNN
	1    7350 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1025 9700 1150
Wire Wire Line
	9700 1150 9275 1150
Connection ~ 7650 1150
Wire Wire Line
	7650 1150 7350 1150
Connection ~ 7950 1150
Wire Wire Line
	7950 1150 7650 1150
Connection ~ 8225 1150
Wire Wire Line
	8225 1150 7950 1150
Connection ~ 8525 1150
Wire Wire Line
	8525 1150 8225 1150
Connection ~ 8775 1150
Wire Wire Line
	8775 1150 8525 1150
Connection ~ 9025 1150
Wire Wire Line
	9025 1150 8775 1150
Connection ~ 9275 1150
Wire Wire Line
	9275 1150 9025 1150
Wire Wire Line
	7350 1450 7650 1450
Connection ~ 7650 1450
Wire Wire Line
	7650 1450 7950 1450
Connection ~ 7950 1450
Wire Wire Line
	7950 1450 8225 1450
Connection ~ 8225 1450
Wire Wire Line
	8225 1450 8525 1450
Connection ~ 8525 1450
Wire Wire Line
	8525 1450 8775 1450
Connection ~ 8775 1450
Wire Wire Line
	8775 1450 9025 1450
Connection ~ 9025 1450
Wire Wire Line
	9025 1450 9275 1450
$Comp
L Device:CP C1
U 1 1 5C13126A
P 2450 1300
F 0 "C1" H 2225 1350 50  0000 L CNN
F 1 "10uF" H 2175 1200 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.9" H 2488 1150 50  0001 C CNN
F 3 "~" H 2450 1300 50  0001 C CNN
	1    2450 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3825 1300 3825 1425
Wire Wire Line
	2300 3325 2400 3325
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J2
U 1 1 5C005556
P 1600 2500
F 0 "J2" H 1650 3017 50  0000 C CNN
F 1 "Conn_02x07_Odd_Even" H 1650 2926 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x07_P2.54mm_Vertical" H 1600 2500 50  0001 C CNN
F 3 "~" H 1600 2500 50  0001 C CNN
	1    1600 2500
	1    0    0    -1  
$EndComp
Connection ~ 1950 1250
Connection ~ 1950 1350
Wire Wire Line
	1950 1050 2100 1050
Wire Wire Line
	1950 1625 1775 1625
Wire Wire Line
	1775 1625 1775 1550
Wire Wire Line
	1950 1350 1950 1625
Wire Wire Line
	1550 1350 1950 1350
Wire Wire Line
	1550 1250 1950 1250
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U8
U 1 1 5C3E3A8E
P 5650 7100
F 0 "U8" H 5650 7525 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 7434 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 7050 50  0001 C CNN
F 3 "" H 5650 7050 50  0001 C CNN
	1    5650 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	9275 1450 9700 1450
Connection ~ 9275 1450
$Comp
L power:GND #PWR014
U 1 1 5C3F33FD
P 9700 1575
F 0 "#PWR014" H 9700 1325 50  0001 C CNN
F 1 "GND" H 9705 1402 50  0000 C CNN
F 2 "" H 9700 1575 50  0001 C CNN
F 3 "" H 9700 1575 50  0001 C CNN
	1    9700 1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1450 9700 1575
Wire Wire Line
	3825 1425 3825 1800
Wire Wire Line
	3575 1425 4000 1425
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U7
U 1 1 5C43CEF3
P 5650 6450
F 0 "U7" H 5650 6875 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 6784 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 6400 50  0001 C CNN
F 3 "" H 5650 6400 50  0001 C CNN
	1    5650 6450
	-1   0    0    1   
$EndComp
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U6
U 1 1 5C43CF8E
P 5650 5800
F 0 "U6" H 5650 6225 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 6134 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 5750 50  0001 C CNN
F 3 "" H 5650 5750 50  0001 C CNN
	1    5650 5800
	-1   0    0    1   
$EndComp
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U5
U 1 1 5C43D16A
P 5650 5150
F 0 "U5" H 5650 5575 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 5484 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 5100 50  0001 C CNN
F 3 "" H 5650 5100 50  0001 C CNN
	1    5650 5150
	-1   0    0    1   
$EndComp
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U4
U 1 1 5C43D171
P 5650 4500
F 0 "U4" H 5650 4925 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 4834 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 4450 50  0001 C CNN
F 3 "" H 5650 4450 50  0001 C CNN
	1    5650 4500
	-1   0    0    1   
$EndComp
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U2
U 1 1 5C4414A5
P 5650 3200
F 0 "U2" H 5650 3625 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 3534 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 3150 50  0001 C CNN
F 3 "" H 5650 3150 50  0001 C CNN
	1    5650 3200
	-1   0    0    1   
$EndComp
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U1
U 1 1 5C4414AC
P 5650 2550
F 0 "U1" H 5650 2975 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 2884 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 2500 50  0001 C CNN
F 3 "" H 5650 2500 50  0001 C CNN
	1    5650 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	4525 2950 5250 2950
Wire Wire Line
	5250 2950 5250 3100
Wire Wire Line
	4525 3450 5250 3450
Wire Wire Line
	5250 3450 5250 3750
Wire Wire Line
	5175 3975 5175 4400
Wire Wire Line
	5175 4400 5250 4400
Wire Wire Line
	4525 3975 5175 3975
Wire Wire Line
	5175 4450 5175 5050
Wire Wire Line
	5175 5050 5250 5050
Wire Wire Line
	4525 4450 5175 4450
Wire Wire Line
	5075 4950 5075 5700
Wire Wire Line
	5075 5700 5250 5700
Wire Wire Line
	4525 4950 5075 4950
Wire Wire Line
	5025 5475 5025 6350
Wire Wire Line
	5025 6350 5250 6350
Wire Wire Line
	4525 5475 5025 5475
Wire Wire Line
	4825 5950 4825 7000
Wire Wire Line
	4825 7000 5250 7000
Wire Wire Line
	6050 2650 6575 2650
$Comp
L power:+5V #PWR012
U 1 1 5C4A297A
P 6575 2200
F 0 "#PWR012" H 6575 2050 50  0001 C CNN
F 1 "+5V" H 6590 2373 50  0000 C CNN
F 2 "" H 6575 2200 50  0001 C CNN
F 3 "" H 6575 2200 50  0001 C CNN
	1    6575 2200
	1    0    0    -1  
$EndComp
$Comp
L pressure_sense_pcb:HSCDAND030PGSA3 U3
U 1 1 5C43D178
P 5650 3850
F 0 "U3" H 5650 4275 50  0000 C CNN
F 1 "HSCDAND030PGSA3" H 5650 4184 50  0000 C CNN
F 2 "pressure_sense_pcb:DIP-8_W13.08mm_Socket" H 5650 3800 50  0001 C CNN
F 3 "" H 5650 3800 50  0001 C CNN
	1    5650 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6575 2200 6575 2650
Wire Wire Line
	6575 2650 6575 3300
Wire Wire Line
	6575 7200 6050 7200
Connection ~ 6575 2650
Wire Wire Line
	6050 6550 6575 6550
Connection ~ 6575 6550
Wire Wire Line
	6575 6550 6575 7200
Wire Wire Line
	6050 5900 6575 5900
Connection ~ 6575 5900
Wire Wire Line
	6575 5900 6575 6550
Wire Wire Line
	6050 5250 6575 5250
Connection ~ 6575 5250
Wire Wire Line
	6575 5250 6575 5900
Wire Wire Line
	6050 4600 6575 4600
Connection ~ 6575 4600
Wire Wire Line
	6575 4600 6575 5250
Wire Wire Line
	6050 3950 6575 3950
Connection ~ 6575 3950
Wire Wire Line
	6575 3950 6575 4600
Wire Wire Line
	6050 3300 6575 3300
Connection ~ 6575 3300
Wire Wire Line
	6575 3300 6575 3950
Wire Wire Line
	6050 7300 6500 7300
Wire Wire Line
	6500 7300 6500 7550
$Comp
L power:GND #PWR011
U 1 1 5C4CD0A8
P 6500 7550
F 0 "#PWR011" H 6500 7300 50  0001 C CNN
F 1 "GND" H 6505 7377 50  0000 C CNN
F 2 "" H 6500 7550 50  0001 C CNN
F 3 "" H 6500 7550 50  0001 C CNN
	1    6500 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 7300 6500 6650
Wire Wire Line
	6500 2750 6050 2750
Connection ~ 6500 7300
Wire Wire Line
	6050 3400 6500 3400
Connection ~ 6500 3400
Wire Wire Line
	6500 3400 6500 2750
Wire Wire Line
	6050 4050 6500 4050
Connection ~ 6500 4050
Wire Wire Line
	6500 4050 6500 3400
Wire Wire Line
	6050 4700 6500 4700
Connection ~ 6500 4700
Wire Wire Line
	6500 4700 6500 4050
Wire Wire Line
	6050 5350 6500 5350
Connection ~ 6500 5350
Wire Wire Line
	6500 5350 6500 4700
Wire Wire Line
	6050 6000 6500 6000
Connection ~ 6500 6000
Wire Wire Line
	6500 6000 6500 5350
Wire Wire Line
	6050 6650 6500 6650
Connection ~ 6500 6650
Wire Wire Line
	6500 6650 6500 6000
Wire Wire Line
	4525 1925 6175 1925
Wire Wire Line
	6175 1925 6175 2450
Wire Wire Line
	6175 2450 6050 2450
Wire Wire Line
	6175 2450 6175 3100
Wire Wire Line
	6175 7000 6050 7000
Connection ~ 6175 2450
Wire Wire Line
	6050 3100 6175 3100
Connection ~ 6175 3100
Wire Wire Line
	6175 3100 6175 3750
Wire Wire Line
	6050 3750 6175 3750
Connection ~ 6175 3750
Wire Wire Line
	6050 4400 6175 4400
Wire Wire Line
	6175 3750 6175 4400
Connection ~ 6175 4400
Wire Wire Line
	6175 4400 6175 5050
Wire Wire Line
	6050 6350 6175 6350
Connection ~ 6175 6350
Wire Wire Line
	6175 6350 6175 7000
Wire Wire Line
	6050 5700 6175 5700
Connection ~ 6175 5700
Wire Wire Line
	6175 5700 6175 6350
Wire Wire Line
	4525 1425 6275 1425
Wire Wire Line
	6275 1425 6275 2550
Wire Wire Line
	6275 2550 6050 2550
Wire Wire Line
	6275 2550 6275 3200
Wire Wire Line
	6275 3200 6050 3200
Connection ~ 6275 2550
Wire Wire Line
	6275 3200 6275 3850
Wire Wire Line
	6275 3850 6050 3850
Connection ~ 6275 3200
Wire Wire Line
	6275 3850 6275 4500
Wire Wire Line
	6275 4500 6050 4500
Connection ~ 6275 3850
Wire Wire Line
	6275 4500 6275 5150
Wire Wire Line
	6275 5150 6050 5150
Connection ~ 6275 4500
Wire Wire Line
	6050 5050 6175 5050
Connection ~ 6175 5050
Wire Wire Line
	6175 5050 6175 5700
Wire Wire Line
	6275 5150 6275 5800
Wire Wire Line
	6275 5800 6050 5800
Connection ~ 6275 5150
Wire Wire Line
	6275 5800 6275 6450
Wire Wire Line
	6275 6450 6050 6450
Connection ~ 6275 5800
Wire Wire Line
	6275 6450 6275 7100
Wire Wire Line
	6275 7100 6050 7100
Connection ~ 6275 6450
Wire Wire Line
	4700 1800 4700 2325
Wire Wire Line
	4525 2325 4700 2325
Connection ~ 4700 2325
Wire Wire Line
	4000 2450 3675 2450
Wire Wire Line
	4000 2325 3825 2325
Connection ~ 3825 2325
$Sheet
S 4000 2225 525  325 
U 5C5CE1FB
F0 "sheet5C5CE1F5" 50
F1 "lshifter_bidirectional.sch" 50
F2 "Vin" I L 4000 2325 50 
F3 "Vout" I R 4525 2325 50 
F4 "IN" B L 4000 2450 50 
F5 "OUT" B R 4525 2450 50 
$EndSheet
Wire Wire Line
	4700 2325 4700 2450
Wire Wire Line
	4700 2450 4700 2825
Wire Wire Line
	4525 2450 5250 2450
$Comp
L power:+5V #PWR02
U 1 1 5C621BBF
P 1600 3200
F 0 "#PWR02" H 1600 3050 50  0001 C CNN
F 1 "+5V" H 1615 3373 50  0000 C CNN
F 2 "" H 1600 3200 50  0001 C CNN
F 3 "" H 1600 3200 50  0001 C CNN
	1    1600 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C621BF0
P 1950 3275
F 0 "#PWR04" H 1950 3025 50  0001 C CNN
F 1 "GND" H 1955 3102 50  0000 C CNN
F 2 "" H 1950 3275 50  0001 C CNN
F 3 "" H 1950 3275 50  0001 C CNN
	1    1950 3275
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR01
U 1 1 5C621C21
P 1200 3200
F 0 "#PWR01" H 1200 3050 50  0001 C CNN
F 1 "+3.3V" H 1215 3373 50  0000 C CNN
F 2 "" H 1200 3200 50  0001 C CNN
F 3 "" H 1200 3200 50  0001 C CNN
	1    1200 3200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5C621CEF
P 1950 3200
F 0 "#FLG03" H 1950 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 1950 3374 50  0000 C CNN
F 2 "" H 1950 3200 50  0001 C CNN
F 3 "~" H 1950 3200 50  0001 C CNN
	1    1950 3200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5C621D27
P 1200 3300
F 0 "#FLG01" H 1200 3375 50  0001 C CNN
F 1 "PWR_FLAG" H 1200 3473 50  0000 C CNN
F 2 "" H 1200 3300 50  0001 C CNN
F 3 "~" H 1200 3300 50  0001 C CNN
	1    1200 3300
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5C621D7E
P 1600 3300
F 0 "#FLG02" H 1600 3375 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 3473 50  0000 C CNN
F 2 "" H 1600 3300 50  0001 C CNN
F 3 "~" H 1600 3300 50  0001 C CNN
	1    1600 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 3200 1200 3300
Wire Wire Line
	1600 3200 1600 3300
Wire Wire Line
	1950 3200 1950 3275
Text Label 2025 2300 0    50   ~ 0
CS5
Text Label 2025 2400 0    50   ~ 0
CS4
Text Label 2025 2500 0    50   ~ 0
CS3
Text Label 2025 2600 0    50   ~ 0
CS2
Text Label 2025 2700 0    50   ~ 0
CS1
Text Label 2025 2800 0    50   ~ 0
CS0
$Comp
L power:+3.3V #PWR0101
U 1 1 5C6A17E0
P 1150 2150
F 0 "#PWR0101" H 1150 2000 50  0001 C CNN
F 1 "+3.3V" H 1165 2323 50  0000 C CNN
F 2 "" H 1150 2150 50  0001 C CNN
F 3 "" H 1150 2150 50  0001 C CNN
	1    1150 2150
	1    0    0    -1  
$EndComp
Text Label 1150 2300 0    50   ~ 0
CLK
Text Label 1150 2400 0    50   ~ 0
MISO
$Comp
L power:GND #PWR0102
U 1 1 5C6A18FD
P 1025 2575
F 0 "#PWR0102" H 1025 2325 50  0001 C CNN
F 1 "GND" H 1030 2402 50  0000 C CNN
F 2 "" H 1025 2575 50  0001 C CNN
F 3 "" H 1025 2575 50  0001 C CNN
	1    1025 2575
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2300 1400 2300
Wire Wire Line
	1150 2400 1400 2400
Wire Wire Line
	1400 2200 1150 2200
Wire Wire Line
	1150 2200 1150 2150
Wire Wire Line
	1025 2575 1025 2500
Wire Wire Line
	1025 2500 1400 2500
Wire Wire Line
	1900 2200 2025 2200
Wire Wire Line
	1900 2300 2025 2300
Wire Wire Line
	1900 2400 2025 2400
Wire Wire Line
	1900 2500 2025 2500
Wire Wire Line
	1900 2600 2025 2600
Wire Wire Line
	1900 2700 2025 2700
Wire Wire Line
	1900 2800 2025 2800
Wire Wire Line
	1400 2800 1175 2800
Text Label 1175 2800 0    50   ~ 0
CS7
NoConn ~ 1400 2600
NoConn ~ 1400 2700
$EndSCHEMATC
