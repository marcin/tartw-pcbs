EESchema Schematic File Version 4
LIBS:pressure_sense_pcb-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 5BF4E4BE
P 2975 2950
AR Path="/5BF4E4BE" Ref="R?"  Part="1" 
AR Path="/5BF4E0A5/5BF4E4BE" Ref="R11"  Part="1" 
AR Path="/5BF4FF81/5BF4E4BE" Ref="R1"  Part="1" 
AR Path="/5BF50362/5BF4E4BE" Ref="R?"  Part="1" 
AR Path="/5BF50882/5BF4E4BE" Ref="R13"  Part="1" 
AR Path="/5BF5088D/5BF4E4BE" Ref="R3"  Part="1" 
AR Path="/5BF50898/5BF4E4BE" Ref="R15"  Part="1" 
AR Path="/5BF51D67/5BF4E4BE" Ref="R5"  Part="1" 
AR Path="/5BF51D72/5BF4E4BE" Ref="R7"  Part="1" 
AR Path="/5BF51D7D/5BF4E4BE" Ref="R17"  Part="1" 
AR Path="/5BF51D88/5BF4E4BE" Ref="R9"  Part="1" 
AR Path="/5C5CE1FB/5BF4E4BE" Ref="R19"  Part="1" 
F 0 "R7" H 3045 2996 50  0000 L CNN
F 1 "10k" H 3045 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2905 2950 50  0001 C CNN
F 3 "~" H 2975 2950 50  0001 C CNN
	1    2975 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2975 3100 2975 3225
Wire Wire Line
	2975 3225 3150 3225
Wire Wire Line
	3550 3225 3675 3225
$Comp
L Device:R R?
U 1 1 5BF4E4CD
P 3675 2950
AR Path="/5BF4E4CD" Ref="R?"  Part="1" 
AR Path="/5BF4E0A5/5BF4E4CD" Ref="R12"  Part="1" 
AR Path="/5BF4FF81/5BF4E4CD" Ref="R2"  Part="1" 
AR Path="/5BF50362/5BF4E4CD" Ref="R?"  Part="1" 
AR Path="/5BF50882/5BF4E4CD" Ref="R14"  Part="1" 
AR Path="/5BF5088D/5BF4E4CD" Ref="R4"  Part="1" 
AR Path="/5BF50898/5BF4E4CD" Ref="R16"  Part="1" 
AR Path="/5BF51D67/5BF4E4CD" Ref="R6"  Part="1" 
AR Path="/5BF51D72/5BF4E4CD" Ref="R8"  Part="1" 
AR Path="/5BF51D7D/5BF4E4CD" Ref="R18"  Part="1" 
AR Path="/5BF51D88/5BF4E4CD" Ref="R10"  Part="1" 
AR Path="/5C5CE1FB/5BF4E4CD" Ref="R20"  Part="1" 
F 0 "R8" H 3745 2996 50  0000 L CNN
F 1 "10k" H 3745 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3605 2950 50  0001 C CNN
F 3 "~" H 3675 2950 50  0001 C CNN
	1    3675 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3675 3100 3675 3225
Connection ~ 3675 3225
Wire Wire Line
	3675 3225 3900 3225
Wire Wire Line
	2700 3225 2975 3225
Connection ~ 2975 3225
Wire Wire Line
	3675 2425 3675 2800
Text HLabel 2700 3225 0    50   BiDi ~ 0
IN
Text HLabel 3900 3225 2    50   BiDi ~ 0
OUT
Text HLabel 2975 2425 1    50   Input ~ 0
Vin
Text HLabel 3675 2425 1    50   Input ~ 0
Vout
NoConn ~ 5775 4050
Wire Wire Line
	2975 2425 2975 2625
Connection ~ 2975 2625
Wire Wire Line
	2975 2625 2975 2800
$Comp
L pressure_sense_pcb:2N7002 Q6
U 1 1 5C665D3C
P 3350 3125
AR Path="/5BF4E0A5/5C665D3C" Ref="Q6"  Part="1" 
AR Path="/5BF4FF81/5C665D3C" Ref="Q1"  Part="1" 
AR Path="/5BF5088D/5C665D3C" Ref="Q2"  Part="1" 
AR Path="/5BF51D67/5C665D3C" Ref="Q3"  Part="1" 
AR Path="/5BF51D72/5C665D3C" Ref="Q4"  Part="1" 
AR Path="/5BF51D88/5C665D3C" Ref="Q5"  Part="1" 
AR Path="/5BF50882/5C665D3C" Ref="Q7"  Part="1" 
AR Path="/5BF50898/5C665D3C" Ref="Q8"  Part="1" 
AR Path="/5BF51D7D/5C665D3C" Ref="Q9"  Part="1" 
AR Path="/5C5CE1FB/5C665D3C" Ref="Q10"  Part="1" 
F 0 "Q4" V 3600 3125 50  0000 C CNN
F 1 "2N7002" V 3691 3125 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3550 3050 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7002.pdf" H 3350 3125 50  0001 L CNN
	1    3350 3125
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 2625 3350 2925
Wire Wire Line
	2975 2625 3350 2625
NoConn ~ 2600 2575
$EndSCHEMATC
