EESchema Schematic File Version 4
LIBS:control-pcb-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Isolator:LTV-847 U1
U 1 1 5BE2C62B
P 5325 2500
F 0 "U1" H 5325 2825 50  0000 C CNN
F 1 "LTV-847" H 5325 2734 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5125 2300 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 5325 2500 50  0001 L CNN
	1    5325 2500
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847 U1
U 2 1 5BE2D470
P 5325 3100
F 0 "U1" H 5325 3425 50  0000 C CNN
F 1 "LTV-847" H 5325 3334 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5125 2900 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 5325 3100 50  0001 L CNN
	2    5325 3100
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847 U1
U 3 1 5BE2DC76
P 5325 3800
F 0 "U1" H 5325 4125 50  0000 C CNN
F 1 "LTV-847" H 5325 4034 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5125 3600 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 5325 3800 50  0001 L CNN
	3    5325 3800
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847 U1
U 4 1 5BE2E5AD
P 5325 4400
F 0 "U1" H 5325 4725 50  0000 C CNN
F 1 "LTV-847" H 5325 4634 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5125 4200 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 5325 4400 50  0001 L CNN
	4    5325 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BE2F03D
P 4625 2400
F 0 "R1" V 4418 2400 50  0000 C CNN
F 1 "220" V 4509 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4555 2400 50  0001 C CNN
F 3 "~" H 4625 2400 50  0001 C CNN
	1    4625 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5BE3005D
P 4625 3000
F 0 "R2" V 4418 3000 50  0000 C CNN
F 1 "220" V 4509 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4555 3000 50  0001 C CNN
F 3 "~" H 4625 3000 50  0001 C CNN
	1    4625 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5BE33A78
P 4625 3700
F 0 "R3" V 4418 3700 50  0000 C CNN
F 1 "220" V 4509 3700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4555 3700 50  0001 C CNN
F 3 "~" H 4625 3700 50  0001 C CNN
	1    4625 3700
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5BE33A7E
P 4625 4300
F 0 "R4" V 4418 4300 50  0000 C CNN
F 1 "220" V 4509 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4555 4300 50  0001 C CNN
F 3 "~" H 4625 4300 50  0001 C CNN
	1    4625 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	5025 2400 4775 2400
Wire Wire Line
	4775 3000 5025 3000
Wire Wire Line
	4775 3700 5025 3700
Wire Wire Line
	4775 4300 5025 4300
$Comp
L power:GNDS #PWR04
U 1 1 5BE39F44
P 4825 4725
F 0 "#PWR04" H 4825 4475 50  0001 C CNN
F 1 "GNDS" H 4830 4552 50  0000 C CNN
F 2 "" H 4825 4725 50  0001 C CNN
F 3 "" H 4825 4725 50  0001 C CNN
	1    4825 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	4825 4500 5025 4500
Wire Wire Line
	5025 3900 4825 3900
Wire Wire Line
	4825 3900 4825 4500
Connection ~ 4825 4500
Wire Wire Line
	4825 4500 4825 4725
Wire Wire Line
	4825 3200 5025 3200
Wire Wire Line
	4825 2600 5025 2600
Wire Wire Line
	4825 2600 4825 3200
Wire Wire Line
	4825 3200 4825 3900
$Comp
L Connector_Generic:Conn_02x05_Counter_Clockwise J1
U 1 1 5BE92DD9
P 3175 3400
F 0 "J1" H 3225 3817 50  0000 C CNN
F 1 "Conn_02x05_Counter_Clockwise" H 3225 3726 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 3175 3400 50  0001 C CNN
F 3 "~" H 3175 3400 50  0001 C CNN
	1    3175 3400
	1    0    0    -1  
$EndComp
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q1
U 1 1 5BE98C9B
P 8325 2625
F 0 "Q1" H 8455 2696 50  0000 L CNN
F 1 "SVD5865NLT4G" H 8455 2605 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 8250 2650 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 8250 2650 50  0001 C CNN
	1    8325 2625
	1    0    0    -1  
$EndComp
$Comp
L power:GNDS #PWR03
U 1 1 5BE98DD0
P 3900 3350
F 0 "#PWR03" H 3900 3100 50  0001 C CNN
F 1 "GNDS" H 3905 3177 50  0000 C CNN
F 2 "" H 3900 3350 50  0001 C CNN
F 3 "" H 3900 3350 50  0001 C CNN
	1    3900 3350
	-1   0    0    -1  
$EndComp
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q3
U 1 1 5BE99A25
P 7625 3925
F 0 "Q3" H 7755 3996 50  0000 L CNN
F 1 "SVD5865NLT4G" H 7755 3905 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7550 3950 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 7550 3950 50  0001 C CNN
	1    7625 3925
	1    0    0    -1  
$EndComp
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q4
U 1 1 5BE99A87
P 7075 4525
F 0 "Q4" H 7205 4596 50  0000 L CNN
F 1 "SVD5865NLT4G" H 7205 4505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7000 4550 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 7000 4550 50  0001 C CNN
	1    7075 4525
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5BE9A137
P 5725 4975
F 0 "R12" H 5795 5021 50  0000 L CNN
F 1 "10k" H 5795 4930 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5655 4975 50  0001 C CNN
F 3 "~" H 5725 4975 50  0001 C CNN
	1    5725 4975
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5BE9A18D
P 6025 4975
F 0 "R11" H 6095 5021 50  0000 L CNN
F 1 "10k" H 6095 4930 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5955 4975 50  0001 C CNN
F 3 "~" H 6025 4975 50  0001 C CNN
	1    6025 4975
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5BE9A1D3
P 6300 4975
F 0 "R10" H 6370 5021 50  0000 L CNN
F 1 "10k" H 6370 4930 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6230 4975 50  0001 C CNN
F 3 "~" H 6300 4975 50  0001 C CNN
	1    6300 4975
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR06
U 1 1 5BE9B69D
P 6175 5350
F 0 "#PWR06" H 6175 5150 50  0001 C CNN
F 1 "GNDPWR" H 6179 5196 50  0000 C CNN
F 2 "" H 6175 5300 50  0001 C CNN
F 3 "" H 6175 5300 50  0001 C CNN
	1    6175 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 5BE9EA25
P 2600 2125
F 0 "J2" H 2800 1800 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 2800 1900 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 2600 2125 50  0001 C CNN
F 3 "~" H 2600 2125 50  0001 C CNN
	1    2600 2125
	-1   0    0    1   
$EndComp
$Comp
L power:GNDPWR #PWR02
U 1 1 5BE9EC7E
P 2900 2300
F 0 "#PWR02" H 2900 2100 50  0001 C CNN
F 1 "GNDPWR" H 2904 2146 50  0000 C CNN
F 2 "" H 2900 2250 50  0001 C CNN
F 3 "" H 2900 2250 50  0001 C CNN
	1    2900 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 2125 2900 2125
Wire Wire Line
	2900 2125 2900 2300
$Comp
L power:+12V #PWR01
U 1 1 5BE9FD4F
P 2900 1850
F 0 "#PWR01" H 2900 1700 50  0001 C CNN
F 1 "+12V" H 2915 2023 50  0000 C CNN
F 2 "" H 2900 1850 50  0001 C CNN
F 3 "" H 2900 1850 50  0001 C CNN
	1    2900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1850 2900 2025
Wire Wire Line
	2800 2025 2900 2025
$Comp
L Device:R R9
U 1 1 5BEA3AF5
P 6600 4975
F 0 "R9" H 6670 5021 50  0000 L CNN
F 1 "10k" H 6670 4930 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6530 4975 50  0001 C CNN
F 3 "~" H 6600 4975 50  0001 C CNN
	1    6600 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 4100 7650 4700
Wire Wire Line
	7650 4700 7400 4700
Wire Wire Line
	8050 3400 8050 4700
Wire Wire Line
	8050 4700 7650 4700
Connection ~ 7650 4700
Wire Wire Line
	8350 2800 8350 4700
Wire Wire Line
	8350 4700 8050 4700
Connection ~ 8050 4700
$Comp
L Device:LED D12
U 1 1 5BEC6CA7
P 8900 5050
F 0 "D12" V 8938 4933 50  0000 R CNN
F 1 "LED" V 8847 4933 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8900 5050 50  0001 C CNN
F 3 "~" H 8900 5050 50  0001 C CNN
	1    8900 5050
	0    1    1    0   
$EndComp
$Comp
L Device:LED D11
U 1 1 5BEC6F18
P 9300 5050
F 0 "D11" V 9338 4933 50  0000 R CNN
F 1 "LED" V 9247 4933 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9300 5050 50  0001 C CNN
F 3 "~" H 9300 5050 50  0001 C CNN
	1    9300 5050
	0    1    1    0   
$EndComp
$Comp
L Device:LED D10
U 1 1 5BEC6F78
P 9675 5050
F 0 "D10" V 9713 4933 50  0000 R CNN
F 1 "LED" V 9622 4933 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9675 5050 50  0001 C CNN
F 3 "~" H 9675 5050 50  0001 C CNN
	1    9675 5050
	0    1    1    0   
$EndComp
$Comp
L Device:LED D9
U 1 1 5BEC6FD8
P 10050 5050
F 0 "D9" V 10088 4933 50  0000 R CNN
F 1 "LED" V 9997 4933 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10050 5050 50  0001 C CNN
F 3 "~" H 10050 5050 50  0001 C CNN
	1    10050 5050
	0    1    1    0   
$EndComp
$Comp
L Device:R R20
U 1 1 5BEC70A6
P 8900 5550
F 0 "R20" H 8970 5596 50  0000 L CNN
F 1 "680" H 8970 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8830 5550 50  0001 C CNN
F 3 "~" H 8900 5550 50  0001 C CNN
	1    8900 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5400 8900 5200
$Comp
L Device:R R19
U 1 1 5BEC8DB8
P 9300 5550
F 0 "R19" H 9370 5596 50  0000 L CNN
F 1 "680" H 9370 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9230 5550 50  0001 C CNN
F 3 "~" H 9300 5550 50  0001 C CNN
	1    9300 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 5BEC8E16
P 9675 5550
F 0 "R18" H 9745 5596 50  0000 L CNN
F 1 "680" H 9745 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9605 5550 50  0001 C CNN
F 3 "~" H 9675 5550 50  0001 C CNN
	1    9675 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 5BEC8E80
P 10050 5550
F 0 "R17" H 10120 5596 50  0000 L CNN
F 1 "680" H 10120 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9980 5550 50  0001 C CNN
F 3 "~" H 10050 5550 50  0001 C CNN
	1    10050 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5400 9300 5200
Wire Wire Line
	9675 5400 9675 5200
Wire Wire Line
	10050 5400 10050 5200
Wire Wire Line
	8900 5700 8900 5850
Wire Wire Line
	8900 5850 9300 5850
Wire Wire Line
	10050 5850 10050 5700
Wire Wire Line
	9675 5700 9675 5850
Connection ~ 9675 5850
Wire Wire Line
	9675 5850 10050 5850
Wire Wire Line
	9300 5700 9300 5850
Connection ~ 9300 5850
Wire Wire Line
	7100 4200 8900 4200
Wire Wire Line
	8900 4200 8900 4900
Wire Wire Line
	7100 4200 7100 4300
Wire Wire Line
	7650 3450 9300 3450
Wire Wire Line
	9300 3450 9300 4900
Wire Wire Line
	7650 3450 7650 3700
Wire Wire Line
	9675 2850 9675 4900
Wire Wire Line
	8050 2850 9450 2850
Wire Wire Line
	8050 2850 8050 3000
Wire Wire Line
	10050 2350 10050 4900
Connection ~ 9300 3450
Wire Wire Line
	8900 4200 10375 4200
Connection ~ 8900 4200
Text Label 2825 3200 0    50   ~ 0
IN0
Text Label 2825 3300 0    50   ~ 0
IN4
Text Label 2825 3400 0    50   ~ 0
IN1
Text Label 2825 3500 0    50   ~ 0
IN5
Text Label 2825 3600 0    50   ~ 0
IN2
Wire Wire Line
	2975 3200 2825 3200
Wire Wire Line
	2825 3300 2975 3300
Wire Wire Line
	2825 3400 2975 3400
Wire Wire Line
	2825 3500 2975 3500
Wire Wire Line
	2825 3600 2975 3600
Wire Wire Line
	4475 2400 4350 2400
Text Label 4350 2400 0    50   ~ 0
IN0
Text Label 4375 3000 0    50   ~ 0
IN1
Wire Wire Line
	4475 3000 4375 3000
Wire Wire Line
	4475 3700 4375 3700
Text Label 4375 3700 0    50   ~ 0
IN2
Text Label 4375 4300 0    50   ~ 0
IN3
Wire Wire Line
	4375 4300 4475 4300
Wire Wire Line
	9000 1975 9000 1725
Wire Wire Line
	9000 2275 9000 2350
Wire Wire Line
	9000 2350 10050 2350
Wire Wire Line
	9450 2275 9450 2850
Connection ~ 9450 2850
Wire Wire Line
	9450 2850 9675 2850
Wire Wire Line
	9925 2275 9925 3450
Wire Wire Line
	9300 3450 9925 3450
Wire Wire Line
	10375 2275 10375 4200
Wire Wire Line
	9450 1975 9450 1725
Wire Wire Line
	9450 1725 9000 1725
Wire Wire Line
	9925 1975 9925 1725
Wire Wire Line
	9925 1725 9450 1725
Connection ~ 9450 1725
Wire Wire Line
	10375 1975 10375 1725
Wire Wire Line
	10375 1725 9925 1725
Connection ~ 9925 1725
NoConn ~ 3475 3200
Text Label 3550 3400 0    50   ~ 0
IN7
Wire Wire Line
	3550 3400 3475 3400
Text Label 3550 3500 0    50   ~ 0
IN3
Wire Wire Line
	3550 3500 3475 3500
Text Label 3550 3600 0    50   ~ 0
IN6
Wire Wire Line
	3550 3600 3475 3600
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5C192D92
P 3225 1850
F 0 "#FLG01" H 3225 1925 50  0001 C CNN
F 1 "PWR_FLAG" H 3225 2024 50  0000 C CNN
F 2 "" H 3225 1850 50  0001 C CNN
F 3 "~" H 3225 1850 50  0001 C CNN
	1    3225 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3225 1850 3225 2025
Wire Wire Line
	3225 2025 2900 2025
Connection ~ 2900 2025
Wire Wire Line
	3900 3300 3900 3350
Wire Wire Line
	3475 3300 3900 3300
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5C1DA340
P 3450 2025
F 0 "#FLG03" H 3450 2100 50  0001 C CNN
F 1 "PWR_FLAG" H 3450 2199 50  0000 C CNN
F 2 "" H 3450 2025 50  0001 C CNN
F 3 "~" H 3450 2025 50  0001 C CNN
	1    3450 2025
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2125 3450 2125
Wire Wire Line
	3450 2125 3450 2025
Connection ~ 2900 2125
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5C1EBAFD
P 4025 3275
F 0 "#FLG02" H 4025 3350 50  0001 C CNN
F 1 "PWR_FLAG" H 4025 3449 50  0000 C CNN
F 2 "" H 4025 3275 50  0001 C CNN
F 3 "~" H 4025 3275 50  0001 C CNN
	1    4025 3275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3300 4025 3300
Wire Wire Line
	4025 3300 4025 3275
Connection ~ 3900 3300
Connection ~ 4825 3900
Wire Wire Line
	8350 2350 9000 2350
Wire Wire Line
	8350 2350 8350 2400
Connection ~ 9000 2350
Wire Wire Line
	5625 4500 5725 4500
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q2
U 1 1 5BE999AD
P 8025 3225
F 0 "Q2" H 8155 3296 50  0000 L CNN
F 1 "SVD5865NLT4G" H 8155 3205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7950 3250 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 7950 3250 50  0001 C CNN
	1    8025 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	5625 3200 6300 3200
Wire Wire Line
	5625 2600 6600 2600
Wire Wire Line
	5725 4825 5725 4500
Connection ~ 5725 4500
Wire Wire Line
	5725 4500 6800 4500
Wire Wire Line
	6025 4825 6025 3900
Wire Wire Line
	5625 3900 6025 3900
Connection ~ 6025 3900
Wire Wire Line
	6025 3900 7350 3900
Wire Wire Line
	6300 4825 6300 3200
Connection ~ 6300 3200
Wire Wire Line
	6300 3200 7750 3200
Wire Wire Line
	6600 4825 6600 2600
Wire Wire Line
	6600 2600 8050 2600
Wire Wire Line
	5725 5125 5725 5200
Wire Wire Line
	5725 5200 6025 5200
Wire Wire Line
	6175 5200 6175 5350
Wire Wire Line
	6175 5200 6300 5200
Wire Wire Line
	6600 5200 6600 5125
Connection ~ 6175 5200
Wire Wire Line
	6300 5125 6300 5200
Connection ~ 6300 5200
Wire Wire Line
	6300 5200 6600 5200
Wire Wire Line
	6025 5125 6025 5200
Connection ~ 6025 5200
Wire Wire Line
	6025 5200 6175 5200
$Comp
L power:+12V #PWR09
U 1 1 5BFFBBDD
P 8075 1375
F 0 "#PWR09" H 8075 1225 50  0001 C CNN
F 1 "+12V" H 8090 1548 50  0000 C CNN
F 2 "" H 8075 1375 50  0001 C CNN
F 3 "" H 8075 1375 50  0001 C CNN
	1    8075 1375
	1    0    0    -1  
$EndComp
Wire Wire Line
	5625 2400 5775 2400
Connection ~ 9000 1725
Wire Wire Line
	8075 1375 8075 1725
Wire Wire Line
	8075 1725 9000 1725
Wire Wire Line
	5625 3000 5775 3000
Wire Wire Line
	5625 3700 5775 3700
Connection ~ 5775 3000
Wire Wire Line
	5625 4300 5775 4300
Wire Wire Line
	5775 3000 5775 3700
Connection ~ 5775 3700
Wire Wire Line
	5775 3700 5775 4300
Wire Wire Line
	7400 4700 7400 5200
Wire Wire Line
	7400 5200 6600 5200
Connection ~ 7400 4700
Wire Wire Line
	7400 4700 7100 4700
Connection ~ 6600 5200
$Comp
L power:+12V #PWR010
U 1 1 5C03977D
P 8425 5900
F 0 "#PWR010" H 8425 5750 50  0001 C CNN
F 1 "+12V" H 8440 6073 50  0000 C CNN
F 2 "" H 8425 5900 50  0001 C CNN
F 3 "" H 8425 5900 50  0001 C CNN
	1    8425 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9475 5850 9475 6050
Wire Wire Line
	9475 6050 8425 6050
Wire Wire Line
	8425 6050 8425 5900
$Comp
L Isolator:LTV-847 U2
U 1 1 5C043685
P 4925 7450
F 0 "U2" H 4925 7775 50  0000 C CNN
F 1 "LTV-847" H 4925 7684 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4725 7250 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 4925 7450 50  0001 L CNN
	1    4925 7450
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847 U2
U 2 1 5C04368C
P 4925 8050
F 0 "U2" H 4925 8375 50  0000 C CNN
F 1 "LTV-847" H 4925 8284 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4725 7850 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 4925 8050 50  0001 L CNN
	2    4925 8050
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847 U2
U 3 1 5C043693
P 4925 8750
F 0 "U2" H 4925 9075 50  0000 C CNN
F 1 "LTV-847" H 4925 8984 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4725 8550 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 4925 8750 50  0001 L CNN
	3    4925 8750
	1    0    0    -1  
$EndComp
$Comp
L Isolator:LTV-847 U2
U 4 1 5C04369A
P 4925 9350
F 0 "U2" H 4925 9675 50  0000 C CNN
F 1 "LTV-847" H 4925 9584 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4725 9150 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-96-0016/LTV-8X7%20series.PDF" H 4925 9350 50  0001 L CNN
	4    4925 9350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5C0436A1
P 4225 7350
F 0 "R5" V 4018 7350 50  0000 C CNN
F 1 "220" V 4109 7350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4155 7350 50  0001 C CNN
F 3 "~" H 4225 7350 50  0001 C CNN
	1    4225 7350
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5C0436A8
P 4225 7950
F 0 "R6" V 4018 7950 50  0000 C CNN
F 1 "220" V 4109 7950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4155 7950 50  0001 C CNN
F 3 "~" H 4225 7950 50  0001 C CNN
	1    4225 7950
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5C0436AF
P 4225 8650
F 0 "R7" V 4018 8650 50  0000 C CNN
F 1 "220" V 4109 8650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4155 8650 50  0001 C CNN
F 3 "~" H 4225 8650 50  0001 C CNN
	1    4225 8650
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5C0436B6
P 4225 9250
F 0 "R8" V 4018 9250 50  0000 C CNN
F 1 "220" V 4109 9250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4155 9250 50  0001 C CNN
F 3 "~" H 4225 9250 50  0001 C CNN
	1    4225 9250
	0    1    1    0   
$EndComp
Wire Wire Line
	4625 7350 4375 7350
Wire Wire Line
	4375 7950 4625 7950
Wire Wire Line
	4375 8650 4625 8650
Wire Wire Line
	4375 9250 4625 9250
$Comp
L power:GNDS #PWR05
U 1 1 5C0436C1
P 4425 9675
F 0 "#PWR05" H 4425 9425 50  0001 C CNN
F 1 "GNDS" H 4430 9502 50  0000 C CNN
F 2 "" H 4425 9675 50  0001 C CNN
F 3 "" H 4425 9675 50  0001 C CNN
	1    4425 9675
	1    0    0    -1  
$EndComp
Wire Wire Line
	4425 9450 4625 9450
Wire Wire Line
	4625 8850 4425 8850
Wire Wire Line
	4425 8850 4425 9450
Connection ~ 4425 9450
Wire Wire Line
	4425 9450 4425 9675
Wire Wire Line
	4425 8150 4625 8150
Wire Wire Line
	4425 7550 4625 7550
Wire Wire Line
	4425 7550 4425 8150
Connection ~ 4425 8150
Wire Wire Line
	4425 8150 4425 8850
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q5
U 1 1 5C0436D1
P 7925 7575
F 0 "Q5" H 8055 7646 50  0000 L CNN
F 1 "SVD5865NLT4G" H 8055 7555 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7850 7600 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 7850 7600 50  0001 C CNN
	1    7925 7575
	1    0    0    -1  
$EndComp
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q7
U 1 1 5C0436D8
P 7225 8875
F 0 "Q7" H 7355 8946 50  0000 L CNN
F 1 "SVD5865NLT4G" H 7355 8855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7150 8900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 7150 8900 50  0001 C CNN
	1    7225 8875
	1    0    0    -1  
$EndComp
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q8
U 1 1 5C0436DF
P 6675 9475
F 0 "Q8" H 6805 9546 50  0000 L CNN
F 1 "SVD5865NLT4G" H 6805 9455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 6600 9500 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 6600 9500 50  0001 C CNN
	1    6675 9475
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5C0436E6
P 5325 9925
F 0 "R16" H 5395 9971 50  0000 L CNN
F 1 "10k" H 5395 9880 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5255 9925 50  0001 C CNN
F 3 "~" H 5325 9925 50  0001 C CNN
	1    5325 9925
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 5C0436ED
P 5625 9925
F 0 "R15" H 5695 9971 50  0000 L CNN
F 1 "10k" H 5695 9880 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5555 9925 50  0001 C CNN
F 3 "~" H 5625 9925 50  0001 C CNN
	1    5625 9925
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5C0436F4
P 5900 9925
F 0 "R14" H 5970 9971 50  0000 L CNN
F 1 "10k" H 5970 9880 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5830 9925 50  0001 C CNN
F 3 "~" H 5900 9925 50  0001 C CNN
	1    5900 9925
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR07
U 1 1 5C0436FB
P 5775 10300
F 0 "#PWR07" H 5775 10100 50  0001 C CNN
F 1 "GNDPWR" H 5779 10146 50  0000 C CNN
F 2 "" H 5775 10250 50  0001 C CNN
F 3 "" H 5775 10250 50  0001 C CNN
	1    5775 10300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5C043701
P 6200 9925
F 0 "R13" H 6270 9971 50  0000 L CNN
F 1 "10k" H 6270 9880 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6130 9925 50  0001 C CNN
F 3 "~" H 6200 9925 50  0001 C CNN
	1    6200 9925
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 9050 7250 9650
Wire Wire Line
	7250 9650 7000 9650
Wire Wire Line
	7650 8350 7650 9650
Wire Wire Line
	7650 9650 7250 9650
Connection ~ 7250 9650
Wire Wire Line
	7950 7750 7950 9650
Wire Wire Line
	7950 9650 7650 9650
Connection ~ 7650 9650
$Comp
L Device:LED D16
U 1 1 5C043710
P 8500 10000
F 0 "D16" V 8538 9883 50  0000 R CNN
F 1 "LED" V 8447 9883 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8500 10000 50  0001 C CNN
F 3 "~" H 8500 10000 50  0001 C CNN
	1    8500 10000
	0    1    1    0   
$EndComp
$Comp
L Device:LED D15
U 1 1 5C043717
P 8900 10000
F 0 "D15" V 8938 9883 50  0000 R CNN
F 1 "LED" V 8847 9883 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8900 10000 50  0001 C CNN
F 3 "~" H 8900 10000 50  0001 C CNN
	1    8900 10000
	0    1    1    0   
$EndComp
$Comp
L Device:LED D14
U 1 1 5C04371E
P 9275 10000
F 0 "D14" V 9313 9883 50  0000 R CNN
F 1 "LED" V 9222 9883 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9275 10000 50  0001 C CNN
F 3 "~" H 9275 10000 50  0001 C CNN
	1    9275 10000
	0    1    1    0   
$EndComp
$Comp
L Device:LED D13
U 1 1 5C043725
P 9650 10000
F 0 "D13" V 9688 9883 50  0000 R CNN
F 1 "LED" V 9597 9883 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9650 10000 50  0001 C CNN
F 3 "~" H 9650 10000 50  0001 C CNN
	1    9650 10000
	0    1    1    0   
$EndComp
$Comp
L Device:R R24
U 1 1 5C04372C
P 8500 10500
F 0 "R24" H 8570 10546 50  0000 L CNN
F 1 "680" H 8570 10455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8430 10500 50  0001 C CNN
F 3 "~" H 8500 10500 50  0001 C CNN
	1    8500 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 10350 8500 10150
$Comp
L Device:R R23
U 1 1 5C043734
P 8900 10500
F 0 "R23" H 8970 10546 50  0000 L CNN
F 1 "680" H 8970 10455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8830 10500 50  0001 C CNN
F 3 "~" H 8900 10500 50  0001 C CNN
	1    8900 10500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 5C04373B
P 9275 10500
F 0 "R22" H 9345 10546 50  0000 L CNN
F 1 "680" H 9345 10455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9205 10500 50  0001 C CNN
F 3 "~" H 9275 10500 50  0001 C CNN
	1    9275 10500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 5C043742
P 9650 10500
F 0 "R21" H 9720 10546 50  0000 L CNN
F 1 "680" H 9720 10455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9580 10500 50  0001 C CNN
F 3 "~" H 9650 10500 50  0001 C CNN
	1    9650 10500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 10350 8900 10150
Wire Wire Line
	9275 10350 9275 10150
Wire Wire Line
	9650 10350 9650 10150
Wire Wire Line
	8500 10650 8500 10800
Wire Wire Line
	8500 10800 8900 10800
Wire Wire Line
	9650 10800 9650 10650
Wire Wire Line
	9275 10650 9275 10800
Connection ~ 9275 10800
Wire Wire Line
	9275 10800 9650 10800
Wire Wire Line
	8900 10650 8900 10800
Connection ~ 8900 10800
Wire Wire Line
	6700 9150 8500 9150
Wire Wire Line
	8500 9150 8500 9850
Wire Wire Line
	6700 9150 6700 9250
Wire Wire Line
	7250 8400 8900 8400
Wire Wire Line
	8900 8400 8900 9850
Wire Wire Line
	7250 8400 7250 8650
Wire Wire Line
	9275 7800 9275 9850
Wire Wire Line
	7650 7800 9050 7800
Wire Wire Line
	7650 7800 7650 7950
Wire Wire Line
	9650 7300 9650 9850
Connection ~ 8900 8400
Wire Wire Line
	8500 9150 9975 9150
Connection ~ 8500 9150
Wire Wire Line
	4075 7350 3950 7350
Text Label 3950 7350 0    50   ~ 0
IN4
Text Label 3975 7950 0    50   ~ 0
IN5
Wire Wire Line
	4075 7950 3975 7950
Wire Wire Line
	4075 8650 3975 8650
Text Label 3975 8650 0    50   ~ 0
IN6
Text Label 3975 9250 0    50   ~ 0
IN7
Wire Wire Line
	3975 9250 4075 9250
Wire Wire Line
	8600 6925 8600 6675
Wire Wire Line
	8600 7225 8600 7300
Wire Wire Line
	8600 7300 9650 7300
Wire Wire Line
	9050 7225 9050 7800
Connection ~ 9050 7800
Wire Wire Line
	9050 7800 9275 7800
Wire Wire Line
	9525 7225 9525 8400
Wire Wire Line
	8900 8400 9525 8400
Wire Wire Line
	9975 7225 9975 9150
Wire Wire Line
	9050 6925 9050 6675
Wire Wire Line
	9050 6675 8600 6675
Wire Wire Line
	9525 6925 9525 6675
Wire Wire Line
	9525 6675 9050 6675
Connection ~ 9050 6675
Wire Wire Line
	9975 6925 9975 6675
Connection ~ 9525 6675
Connection ~ 4425 8850
Wire Wire Line
	7950 7300 8600 7300
Wire Wire Line
	7950 7300 7950 7350
Connection ~ 8600 7300
Wire Wire Line
	5225 9450 5325 9450
$Comp
L control-pcb-rescue:SVD5865NLT4G-control-pcb Q6
U 1 1 5C0437A7
P 7625 8175
F 0 "Q6" H 7755 8246 50  0000 L CNN
F 1 "SVD5865NLT4G" H 7755 8155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7550 8200 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/NVD5865NL-D.PDF" H 7550 8200 50  0001 C CNN
	1    7625 8175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5225 8150 5900 8150
Wire Wire Line
	5225 7550 6200 7550
Wire Wire Line
	5325 9775 5325 9450
Connection ~ 5325 9450
Wire Wire Line
	5325 9450 6400 9450
Wire Wire Line
	5625 9775 5625 8850
Wire Wire Line
	5225 8850 5625 8850
Connection ~ 5625 8850
Wire Wire Line
	5625 8850 6950 8850
Wire Wire Line
	5900 9775 5900 8150
Connection ~ 5900 8150
Wire Wire Line
	5900 8150 7350 8150
Wire Wire Line
	6200 9775 6200 7550
Connection ~ 6200 7550
Wire Wire Line
	6200 7550 7650 7550
Wire Wire Line
	5325 10075 5325 10150
Wire Wire Line
	5325 10150 5625 10150
Wire Wire Line
	5775 10150 5775 10300
Wire Wire Line
	5775 10150 5900 10150
Wire Wire Line
	6200 10150 6200 10075
Connection ~ 5775 10150
Wire Wire Line
	5900 10075 5900 10150
Connection ~ 5900 10150
Wire Wire Line
	5900 10150 6200 10150
Wire Wire Line
	5625 10075 5625 10150
Connection ~ 5625 10150
Wire Wire Line
	5625 10150 5775 10150
$Comp
L power:+12V #PWR08
U 1 1 5C0437C9
P 7675 6325
F 0 "#PWR08" H 7675 6175 50  0001 C CNN
F 1 "+12V" H 7690 6498 50  0000 C CNN
F 2 "" H 7675 6325 50  0001 C CNN
F 3 "" H 7675 6325 50  0001 C CNN
	1    7675 6325
	1    0    0    -1  
$EndComp
Wire Wire Line
	5225 7350 5375 7350
Connection ~ 8600 6675
Wire Wire Line
	7675 6325 7675 6675
Wire Wire Line
	7675 6675 8600 6675
Wire Wire Line
	5225 7950 5375 7950
Wire Wire Line
	5225 8650 5375 8650
Wire Wire Line
	5375 7350 5375 7950
Connection ~ 5375 7950
Wire Wire Line
	5225 9250 5375 9250
Wire Wire Line
	5375 7950 5375 8650
Connection ~ 5375 8650
Wire Wire Line
	5375 8650 5375 9250
Wire Wire Line
	7000 9650 7000 10150
Wire Wire Line
	7000 10150 6200 10150
Connection ~ 7000 9650
Wire Wire Line
	7000 9650 6700 9650
Connection ~ 6200 10150
Wire Wire Line
	8900 10800 9075 10800
$Comp
L power:+12V #PWR011
U 1 1 5C0437E5
P 8025 10850
F 0 "#PWR011" H 8025 10700 50  0001 C CNN
F 1 "+12V" H 8040 11023 50  0000 C CNN
F 2 "" H 8025 10850 50  0001 C CNN
F 3 "" H 8025 10850 50  0001 C CNN
	1    8025 10850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9075 10800 9075 11000
Wire Wire Line
	9075 11000 8025 11000
Wire Wire Line
	8025 11000 8025 10850
Connection ~ 9075 10800
Wire Wire Line
	9075 10800 9275 10800
$Comp
L Diode:MRA4004T3G D1
U 1 1 5C3C94B4
P 9000 2125
F 0 "D1" V 8954 2204 50  0000 L CNN
F 1 "MRA4004T3G" V 9045 2204 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 9000 1950 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 9000 2125 50  0001 C CNN
	1    9000 2125
	0    1    1    0   
$EndComp
$Comp
L Diode:MRA4004T3G D2
U 1 1 5C421FD5
P 9450 2125
F 0 "D2" V 9404 2204 50  0000 L CNN
F 1 "MRA4004T3G" V 9495 2204 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 9450 1950 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 9450 2125 50  0001 C CNN
	1    9450 2125
	0    1    1    0   
$EndComp
$Comp
L Diode:MRA4004T3G D3
U 1 1 5C422079
P 9925 2125
F 0 "D3" V 9879 2204 50  0000 L CNN
F 1 "MRA4004T3G" V 9970 2204 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 9925 1950 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 9925 2125 50  0001 C CNN
	1    9925 2125
	0    1    1    0   
$EndComp
$Comp
L Diode:MRA4004T3G D4
U 1 1 5C422127
P 10375 2125
F 0 "D4" V 10329 2204 50  0000 L CNN
F 1 "MRA4004T3G" V 10420 2204 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 10375 1950 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 10375 2125 50  0001 C CNN
	1    10375 2125
	0    1    1    0   
$EndComp
$Comp
L Diode:MRA4004T3G D5
U 1 1 5C4471EA
P 8600 7075
F 0 "D5" V 8554 7154 50  0000 L CNN
F 1 "MRA4004T3G" V 8645 7154 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 8600 6900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 8600 7075 50  0001 C CNN
	1    8600 7075
	0    1    1    0   
$EndComp
$Comp
L Diode:MRA4004T3G D6
U 1 1 5C4471F1
P 9050 7075
F 0 "D6" V 9004 7154 50  0000 L CNN
F 1 "MRA4004T3G" V 9095 7154 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 9050 6900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 9050 7075 50  0001 C CNN
	1    9050 7075
	0    1    1    0   
$EndComp
$Comp
L Diode:MRA4004T3G D7
U 1 1 5C4471F8
P 9525 7075
F 0 "D7" V 9479 7154 50  0000 L CNN
F 1 "MRA4004T3G" V 9570 7154 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 9525 6900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 9525 7075 50  0001 C CNN
	1    9525 7075
	0    1    1    0   
$EndComp
$Comp
L Diode:MRA4004T3G D8
U 1 1 5C4471FF
P 9975 7075
F 0 "D8" V 9929 7154 50  0000 L CNN
F 1 "MRA4004T3G" V 10020 7154 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 9975 6900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MRA4003T3-D.PDF" H 9975 7075 50  0001 C CNN
	1    9975 7075
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J6
U 1 1 5C4ABD69
P 11300 4200
F 0 "J6" H 11220 3875 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11220 3966 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11300 4200 50  0001 C CNN
F 3 "~" H 11300 4200 50  0001 C CNN
	1    11300 4200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 5C4C55C6
P 11300 3450
F 0 "J5" H 11220 3125 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11220 3216 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11300 3450 50  0001 C CNN
F 3 "~" H 11300 3450 50  0001 C CNN
	1    11300 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 5C4C565E
P 11300 2850
F 0 "J4" H 11380 2842 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 11380 2751 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11300 2850 50  0001 C CNN
F 3 "~" H 11300 2850 50  0001 C CNN
	1    11300 2850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J8
U 1 1 5C4EB0CD
P 11825 7300
F 0 "J8" H 11745 6975 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11745 7066 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11825 7300 50  0001 C CNN
F 3 "~" H 11825 7300 50  0001 C CNN
	1    11825 7300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J9
U 1 1 5C4EB217
P 11825 7800
F 0 "J9" H 11745 7475 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11745 7566 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11825 7800 50  0001 C CNN
F 3 "~" H 11825 7800 50  0001 C CNN
	1    11825 7800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J10
U 1 1 5C4EB2BD
P 11825 8400
F 0 "J10" H 11745 8075 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11745 8166 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11825 8400 50  0001 C CNN
F 3 "~" H 11825 8400 50  0001 C CNN
	1    11825 8400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J7
U 1 1 5C4EB3E7
P 11800 9150
F 0 "J7" H 11720 8825 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11720 8916 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11800 9150 50  0001 C CNN
F 3 "~" H 11800 9150 50  0001 C CNN
	1    11800 9150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 7300 11625 7300
Connection ~ 9650 7300
Wire Wire Line
	9275 7800 11625 7800
Connection ~ 9275 7800
Wire Wire Line
	9975 9150 11600 9150
Connection ~ 9975 9150
Wire Wire Line
	9525 8400 11625 8400
Connection ~ 9525 8400
Wire Wire Line
	10050 2350 11100 2350
Connection ~ 10050 2350
Wire Wire Line
	9675 2850 11100 2850
Connection ~ 9675 2850
Wire Wire Line
	9925 3450 11100 3450
Connection ~ 9925 3450
Wire Wire Line
	10375 4200 11100 4200
Connection ~ 10375 4200
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5C4C56F8
P 11300 2350
F 0 "J3" H 11220 2025 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 11220 2116 50  0000 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MPT-0,5-2-2.54_1x02_P2.54mm_Horizontal" H 11300 2350 50  0001 C CNN
F 3 "~" H 11300 2350 50  0001 C CNN
	1    11300 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10375 1725 10975 1725
Wire Wire Line
	10975 1725 10975 2450
Wire Wire Line
	10975 2450 11100 2450
Connection ~ 10375 1725
Wire Wire Line
	10975 2450 10975 2950
Wire Wire Line
	10975 2950 11100 2950
Connection ~ 10975 2450
Wire Wire Line
	10975 2950 10975 3550
Wire Wire Line
	10975 3550 11100 3550
Connection ~ 10975 2950
Wire Wire Line
	10975 3550 10975 4300
Wire Wire Line
	10975 4300 11100 4300
Connection ~ 10975 3550
Wire Wire Line
	11625 7400 11500 7400
Wire Wire Line
	11500 7400 11500 6675
Wire Wire Line
	9525 6675 9975 6675
Connection ~ 9975 6675
Wire Wire Line
	9975 6675 11500 6675
Wire Wire Line
	11500 7400 11500 7900
Wire Wire Line
	11500 7900 11625 7900
Connection ~ 11500 7400
Wire Wire Line
	11500 7900 11500 8500
Wire Wire Line
	11500 8500 11625 8500
Connection ~ 11500 7900
Wire Wire Line
	11500 8500 11500 9250
Wire Wire Line
	11500 9250 11600 9250
Connection ~ 11500 8500
$Comp
L Device:R R25
U 1 1 5C3E5272
P 4150 2250
F 0 "R25" V 4357 2250 50  0000 C CNN
F 1 "1k" V 4266 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4080 2250 50  0001 C CNN
F 3 "~" H 4150 2250 50  0001 C CNN
	1    4150 2250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R26
U 1 1 5C3E54C8
P 4150 1800
F 0 "R26" V 4357 1800 50  0000 C CNN
F 1 "1k" V 4266 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4080 1800 50  0001 C CNN
F 3 "~" H 4150 1800 50  0001 C CNN
	1    4150 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	4150 1950 4150 2025
$Comp
L power:GNDPWR #PWR0101
U 1 1 5C47E267
P 4150 2550
F 0 "#PWR0101" H 4150 2350 50  0001 C CNN
F 1 "GNDPWR" H 4154 2396 50  0000 C CNN
F 2 "" H 4150 2500 50  0001 C CNN
F 3 "" H 4150 2500 50  0001 C CNN
	1    4150 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2400 4150 2550
Wire Wire Line
	4150 2025 3700 2025
Wire Wire Line
	3700 2025 3700 1850
Connection ~ 4150 2025
Wire Wire Line
	4150 2025 4150 2100
Text Label 3700 1850 0    50   ~ 0
6V
Text Label 5775 2100 0    50   ~ 0
6V
Text Label 5375 7000 0    50   ~ 0
6V
Wire Wire Line
	5375 7350 5375 7000
Connection ~ 5375 7350
$Comp
L power:+12V #PWR?
U 1 1 5C4CB8DA
P 4150 1525
F 0 "#PWR?" H 4150 1375 50  0001 C CNN
F 1 "+12V" H 4165 1698 50  0000 C CNN
F 2 "" H 4150 1525 50  0001 C CNN
F 3 "" H 4150 1525 50  0001 C CNN
	1    4150 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1650 4150 1525
Connection ~ 9475 5850
Wire Wire Line
	9475 5850 9675 5850
Wire Wire Line
	9300 5850 9475 5850
Connection ~ 5775 2400
Wire Wire Line
	5775 2400 5775 2100
Wire Wire Line
	5775 2400 5775 3000
Connection ~ 6600 2600
Connection ~ 4825 3200
$Sheet
S 1050 7575 1575 1225
U 5CA2771E
F0 "one_channel" 50
F1 "one_channel.sch" 50
$EndSheet
$EndSCHEMATC
